.PHONY: make compile

make: objs combine run

objs:
	gcc src/*.c -c -g
	mv ./*.o ./obj

combine:
	gcc obj/*.o obj/util/build/util.o -g -o app
run:
	./app

test:
	gcc tests/*.c -o testapp
	./testapp

clean:
	rm -rf ./app ./testapp ./obj/*.o ./libutil.dylib

util:
	gcc -c -o obj/util/build/util.o src/util/util.c
dev:
	python3.11 devsrc/dterm.py

compile: objs combine