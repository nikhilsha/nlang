#include "util.h"
#include <string.h>

void clear(char* c) {
    strncpy(c, "\0", 1);
}
int strnsame(char* s1, char* s2, int l) {
    return strncmp(s1, s2, l) == 0;
}
int strsame(char* s1, char* s2) {
    return (strncmp(s1, s2, strlen(s1) < strlen(s2) ? strlen(s1) : strlen(s2)) == 0) && strlen(s1) == strlen(s2);
}

int strstarts(char* s1, char* s2) {
    return strnsame(s1, s2, strlen(s2));
}

void strremove(char* str, int n) {
    int len = strlen(str);
    if (n <= len) {
        memmove(str, str+n, len-n+1);
    } else {
        str[0] = '\0';
    }
}

char* as_bool_string(int bit) {
    return &((bit ? "True " : "False")[0]);
}