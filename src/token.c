#include "token.h"
#include "util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define tokencase(t, st) case t: return st
#define addtoken() tokens[last_token_pos] = add; clear(buffer); last_token_pos++;

Token NLANG_UNDEFINED_TOKEN = { ERROR_TOKEN, UNDEFINED };

int tokenize_bufferlog = 0;
int tokenize_tokenlog = 0;

char* TOKENIZE_BUFFERLOG_STRING = "tokenize_bufferlog  ";
char* TOKENIZE_TOKENLOG_STRING = "tokenize_tokenlog  ";

void init_tokenizer() {

    FILE *fp;
    char line[50];

    fp = fopen("info/info.dat", "r");
    if (fp == NULL) {
        printf("Error opening config file\n");
        return;
    }

    while (fgets(line, 50, fp) != NULL) {
        if (strstarts(line, TOKENIZE_BUFFERLOG_STRING)) {
            strremove(line,strlen(TOKENIZE_BUFFERLOG_STRING));
            tokenize_bufferlog = atoi(line);
        } else if (strstarts(line, TOKENIZE_TOKENLOG_STRING)) {
            strremove(line, strlen(TOKENIZE_TOKENLOG_STRING));
            tokenize_tokenlog = atoi(line);
        }
    }

    fclose(fp);
}

Token* tokenize(char* line, int NO_IDS, int* len) {
    int enc_l = 0; // Skip this many characters
    int in_string = 0;
    int in_int = 0;
    int in_id = 0;
    int string_pos = 0;
    int last_token_pos = 0;
    int buflen = 0;
    char* int_buffer = malloc(sizeof(char)*MAX_STRING_SIZE);

    char* buffer = malloc(sizeof(char)*MAX_TOKEN_LENGTH);
    Token tokens[MAX_TOKENIZATION] = {[0 ... MAX_TOKENIZATION-1] = NLANG_UNDEFINED_TOKEN};

    for (int i = 0; i < strlen(line); i++) {

        if (enc_l > 0) {
            enc_l -= 1;
            continue;
        }

        strncat(buffer, &(line[i]), 1);
        buflen = strlen(buffer);
        if (tokenize_bufferlog && !NO_IDS) printf("%s\n", buffer);

        if (in_string && !(buffer[buflen - 1] == '"')) continue;
        if (in_int) {
            if (!isdigit(line[i])) {
                in_int = 0;
                Token add = init_token_full(INT_VALUE, "");
                add.data = malloc(sizeof(char)*MAX_STRING_SIZE);
                strncpy(add.data, int_buffer, strlen(int_buffer)+1);
                addtoken();

                strncat(buffer, &(line[i]), 1); // Prevent the next token from being missed
            } else {
                strncat(int_buffer, &(line[i]), 1);
                continue;
            }
        }

        /*  */ if (strsame("string", buffer)) {
            Token add = init_token(STRING_TYPE);
            addtoken();
        } else if (strsame("=", buffer)) {
            Token add = init_token(EQUALS);
            addtoken();
        } else if (strsame(" ", buffer)) {
            Token add = init_token(SPACE);
            addtoken();
        } else if (strsame("\"", buffer) || buffer[buflen-1] == '"') {
            in_string = !in_string;
            if (!in_string) {
                Token add = init_token_full(STRING_VALUE, "");
                add.data = malloc(sizeof(char)*MAX_STRING_SIZE);
                strncpy(add.data, buffer, buflen+2);
                addtoken();
            }

        } else if (strsame("\n", buffer)) {
            Token add = init_token(NEWLINE);
            addtoken();
        } else if (strsame(";", buffer)) {
            Token add = init_token(SEMICOLON);
            addtoken();
        } else if (strsame(":", buffer)) {
            Token add = init_token(COLON);
            addtoken();
        } else if (strsame(":", buffer)) {
            Token add = init_token(COLON);
            addtoken();
        } else if (strsame("int", buffer)) {
            Token add = init_token(INT_TYPE);
            addtoken();
        } else if (strsame("float", buffer)) {
            Token add = init_token(FLOAT_TYPE);
            addtoken();
        } else if (strsame("bool", buffer)) {
            Token add = init_token(BOOL_TYPE);
            addtoken();
        } else if (isdigit(buffer[0]) || isdigit(line[i])) {
            in_int = 1;
            strncat(int_buffer, &(line[i]), 1);
            continue;
        } else if (strsame("(", buffer)) {
            Token add = init_token(LPAREN);
            addtoken();
        } else if (strsame(")", buffer)) {
            Token add = init_token(RPAREN);
            addtoken();
        } else if (isalpha(line[i]) && !NO_IDS) {
            char* bufcpy = malloc(sizeof(char)*(buflen+MAX_TOKEN_LENGTH+1));
            strncpy(bufcpy, buffer, buflen);
            strncpy(bufcpy, &(line[i]), MAX_TOKEN_LENGTH - 1);
            bufcpy[MAX_TOKEN_LENGTH - 1] = '\0';
            if (is_valid_id(bufcpy)) {
                Token add = init_token_full(IDENTIFIER, "");
                add.data = malloc(sizeof(char)*(buflen+1));
                strncpy(add.data, bufcpy, buflen);
                addtoken();
            }
            free(bufcpy);
        }
    }

    if (tokenize_tokenlog && !NO_IDS) {
        for (int i = 0; i < last_token_pos+1; i++) {
            if (tokens[i].data == NULL || strsame(tokens[i].data, "undefined")) {
                printf("(%s)\n", to_readable_token(tokens[i]));
            } else {
                printf("(%s) with data: %s\n", to_readable_token(tokens[i]), tokens[i].data);
            }
        }
    }

    if (len != NULL) *len = last_token_pos;

    Token* arr = malloc(sizeof(Token)*(last_token_pos+1));
    for (int i = 0; i < last_token_pos; i++) {
        arr[i] = tokens[i];
    }
    return arr;
}

Token init_token(TokenType type) {
    Token rval = {
        type,
        NULL
    };
    return rval;
}
Token init_token_full(TokenType type, char* data) {
    Token rval = {
        type,
        data
    };
    return rval;
}
char* to_readable_token(Token t) {
    switch (t.type)
    {
    tokencase(ERROR_TOKEN, "ERROR TOKEN");
    tokencase(INT_TYPE, "Int Type Token");
    tokencase(STRING_TYPE, "String Type Token");
    tokencase(EQUALS, "= Token");
    tokencase(PLUS, "+ Token");
    tokencase(MINUS, "- Token");
    tokencase(ASTERISK, "* Token");
    tokencase(SLASH, "/ Token");
    tokencase(GREATER, "> Token");
    tokencase(LESS, "< Token");
    tokencase(LESS_E, "<= Token");
    tokencase(GREATER_E, ">= Token");
    tokencase(BOOL_TYPE, "Bool Type Token");
    tokencase(PRINT_CALL, "Printf Token");
    tokencase(SPACE, "Space Token");
    tokencase(TAB, "Tab Token");
    tokencase(NEWLINE, "Newline Token");
    tokencase(IDENTIFIER, "ID Token");
    tokencase(STRING_VALUE, "String Value Token");
    tokencase(SEMICOLON, "Semicolon Token");
    tokencase(COLON, "Color Token");
    tokencase(INT_VALUE, "Int Value Token");
    tokencase(LPAREN, "Left Parentheses");
    tokencase(RPAREN, "Right Parentheses");
    tokencase(NLANG_EOF, "EOF or EOS token; \\0");

    default:
        return "ERROR TOKEN";
    }
}

int is_valid_id(char* str) {
    int length = strlen(str);

    // Check if the identifier is non-empty
    if (length == 0) {
        return 0;
    }

    if (is_token(str)) return 0;

    // Check if the first character is a letter or underscore
    if (!isalpha(str[0]) && str[0] != '_') {
        return 0;
    }

    // Check if the remaining characters are letters, digits, or underscores
    for (int i = 1; i < length; i++) {
        if (!isalnum(str[i]) && str[i] != '_') {
            return 0;
        }
    }

    return 1;
}

int is_token(char* identifier) {
    int len;
    tokenize(identifier, 1, &len);
    int result = (len > 0) ? 1 : 0;
    return result;
}