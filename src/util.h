#ifndef UTIL_H
#define UTIL_H

#include <string.h>

void clear(char*);
int strnsame(char*, char*, int);
int strsame(char*, char*);
int strstarts(char*, char*);
void strremove(char*, int);
char* as_bool_string(int);

#endif