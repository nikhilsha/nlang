#ifndef TOKEN_H
#define TOKEN_H

#define MAX_TOKEN_LENGTH 20
#define MAX_TOKENIZATION 120
#define MAX_STRING_SIZE 20
#define UNDEFINED "undefined"


enum _TokenType {
    ERROR_TOKEN,
    INT_TYPE,
    STRING_TYPE,
    EQUALS,
    PLUS,
    MINUS,
    ASTERISK,
    SLASH,
    GREATER,
    LESS,
    LESS_E,
    GREATER_E,
    BOOL_TYPE,
    PRINT_CALL,
    SPACE,
    TAB,
    NEWLINE,
    IDENTIFIER,
    STRING_VALUE,
    SEMICOLON,
    COLON,
    INT_VALUE,
    BOOL_VALUE,
    FLOAT_TYPE,
    FLOAT_VALUE,
    LPAREN,
    RPAREN,
    NLANG_EOF
};

typedef enum _TokenType TokenType;

#define NLANG_EOS EOF

struct _Token
{
    TokenType type;
    char* data;
};

extern struct _Token NLANG_UNDEFINED_TOKEN;

typedef struct _Token Token;

Token* tokenize(char*, int, int*);
Token init_token(TokenType);
Token init_token_full(TokenType, char*);
void log_token(Token*);
char* to_readable_token(Token);
void init_tokenizer();
int is_valid_id(char*);
int is_token(char*);

#endif